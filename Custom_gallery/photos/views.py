from audioop import reverse
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from .models import Picture, Comment, Like
from .forms import Login

# Create your views here.

def home(request):
    pics = Picture.objects.filter(available=True).all()
    context = {"pictures":pics}
    return render(request,"photos/home.html", context)

def view_photo(request, pk):
    pic = Picture.objects.get(id=pk)
    context = {"picture":pic}
    return render(request, "photos/display_photo.html", context)

def send_comment(request, pk):

    if request.method == "POST":
        comment = request.POST['comment']
        name = request.POST['pname']

        if comment != "" and name != "":
            pic = Picture.objects.get(id=pk)
            Comment.objects.create(name=name, body=comment, picture=pic)

    return redirect("view_photo",pk)

def send_like(request, pk):
    if request.method == "POST":
        name = request.POST['pname']

        if name != "":
            pic = Picture.objects.get(id=pk)
            counter = Like.objects.filter(name=name, picture=pic).count()

            if counter == 0:
                Like.objects.create(name=name, picture=pic)
            
    return redirect("view_photo",pk)

def add_photo(request):
    if request.method == "POST":
        
        pics = request.FILES.getlist('pictures')
        error_list = []
        for pic in pics:
            fname = pic.name
            dots = fname.split(".")

            if len(dots) != 2:
                error_list.append(pic.name)
            elif dots[1] not in ["jpg","jpeg","svg","png"]:
                error_list.append(pic.name)
            else:
                Picture.objects.create(image=pic, available=False)

        if len(error_list) == 0:
            return redirect("home")
        else:
            return render(request, "photos/add_photo.html", context={"errors":error_list})

    return render(request, "photos/add_photo.html")

def __logout(request):
    logout(request)
    return redirect('home')

@login_required(login_url='/login/')
def delete_photo(request, pk):
    pic = Picture.objects.get(pk=pk)
    pic.delete()

    return redirect("home")

@login_required(login_url='/login/')
def accept_pic(request, pk):
    pic = Picture.objects.get(pk=pk)
    pic.available = True
    pic.save()

    return redirect("superuser")

@login_required(login_url='/login/')
def reject_pic(request, pk):
    pic = Picture.objects.get(pk=pk)
    pic.delete()

    return redirect("superuser")

@login_required(login_url='/login/')
def superuser(request):
    pics = Picture.objects.filter(available=False).all()
    context = {"pictures":pics}

    return render(request, "photos/superuser.html", context=context)

def __login(request):
    if request.method == "POST":

        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('superuser')
        else:
            form = Login()
            return render(request, "photos/login.html", context={"form":form,"form_errors":True})

    if request.user.is_authenticated:
        return redirect("superuser")

    form = Login()
    return render(request, "photos/login.html", context={"form":form})
