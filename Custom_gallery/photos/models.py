from django.db import models

# Create your models here.

class Picture(models.Model):
    image = models.ImageField(
                              null=False,
                              blank=False
                              )
    available = models.BooleanField(default=False)

class Like(models.Model):
    name = models.CharField(max_length=50)
    date_added = models.DateTimeField(auto_now_add=True)
    picture = models.ForeignKey(Picture, related_name="likes", on_delete=models.CASCADE)

class Comment(models.Model):
    name = models.CharField(max_length=50)
    body = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)
    picture = models.ForeignKey(Picture, related_name="comments", on_delete=models.CASCADE)