from django.contrib import admin

# Register your models here.

from .models import Picture, Comment, Like

admin.site.register(Picture)
admin.site.register(Comment)
admin.site.register(Like)

