from django.urls import path
from . import views

urlpatterns = [
    path("", views.home, name="home"),
    path("add_photo/", views.add_photo, name="add_photo"),
    path("login/", views.__login, name="login"),
    path("logout/", views.__logout, name="logout"),
    path("view_photo/<str:pk>/", views.view_photo, name="view_photo"),
    path("accept_pic/<str:pk>/", views.accept_pic, name="accept_pic"),
    path("reject_pic/<str:pk>/", views.reject_pic, name="reject_pic"),
    path("superuser/", views.superuser, name="superuser"),
    path("send_comment/<str:pk>/", views.send_comment, name="send_comment"),
    path("send_like/<str:pk>/", views.send_like, name="send_like"),
    path("delete_photo/<str:pk>/", views.delete_photo, name="delete_photo"),
]
