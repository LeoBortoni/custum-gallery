# PLEASE GO TO THE PDF VERSION, IT HAS PICTURES

# Custom-gallery

This project designs a custom gallery that follows the following requirements:

* Allows users to push an image to the gallery.
* Just authorized users can approve or reject new images.
* Just authorized users can delete an image from the gallery.
* Users can log-in and become authorized users, if they know the correct password and user name.
* Users can like and comment images.

# Deployed application

This application is deployed in google cloud platform. You can access it through the following link:

> http://34.121.158.50/

The images uploaded are stored in AWS S3. You might check it copying the image link from some picture from the application in the link above. The link copied shows an AWS S3 service.

# Tests and assumptions

Assuming that the application had just began, and the gallery is empty, when accessing the address http://34.121.158.50/ one will face the following:

![image-20220609214037690](/home/leonardo/.config/Typora/typora-user-images/image-20220609214037690.png)

At this point you are an user, not authorized.

### Uploading an image

In order to populate the gallery, the user must click the button 'Add photo', and will be redirected to the following page:

![image-20220609214236222](/home/leonardo/.config/Typora/typora-user-images/image-20220609214236222.png)

The uploading of images must follow the assumptions:

* The file uploaded must be some of the types: jpg, jpeg, svg or png.
* The file must be smaller than 100MB
* The file must not have dots in its name (saving the extension dot)

If the user provides a file either with dots in the name, or a different format, an error message will appear in red, as following:

![image-20220609214726725](/home/leonardo/.config/Typora/typora-user-images/image-20220609214726725.png)

![image-20220609214749168](/home/leonardo/.config/Typora/typora-user-images/image-20220609214749168.png)

If the file is larger than 100MB, a 413 error page will appear.

If the file is correct, hit 'Submit' and the user will be redirected to the home page.

The picture just pushed will not be displayed in the gallery. The authorized user must accept it first.

### Logging as authorized user

In order to log-in, click the 'Login' button located in the home page. You will be redirected to the log-in page:

![image-20220609215256254](/home/leonardo/.config/Typora/typora-user-images/image-20220609215256254.png)

If you provide the wrong username or password, a warning will appear, as following:

![image-20220609215436952](/home/leonardo/.config/Typora/typora-user-images/image-20220609215436952.png)

The correct username and password is hardset in the database of this application. You will not find it in the code. Yet I will provide the correct username and password, for the sake of fun.

username: FitzChivalry

password: fitzandthefool

These names are from a fantastic book series written by Robin Hobb, check out if you will. When providing the correct username and password, click in 'login', and you will be redirected to a approve/reject page:

![image-20220609220158926](/home/leonardo/.config/Typora/typora-user-images/image-20220609220158926.png)

![image-20220609220654950](/home/leonardo/.config/Typora/typora-user-images/image-20220609220654950.png)

If you try to access this route without authorization, you will be redirected to the login page. It is not possible to perform any 'superuser' action without being logged. In the Accept/reject page, you may click to either accept or reject an image. If you reject, the picture will vanish from the gallery application. If you accept, the picture will be displayed in the gallery.

If you hit 'Return', you will be redirected to the home page. But note, from now you are authorized. The application will set some cookies that will keep your credentials set. Depending on the browser, even if you close the browser, open again, and reload the web page, you are going to still be logged. When using firefox there is an option to clean cookies whenever the browser is closed.

But you will ever know whether you are logged in or not, due to some functionalities that just appear if you are logged in. For instance, after hitting the 'return' button:

![image-20220609221505900](/home/leonardo/.config/Typora/typora-user-images/image-20220609221505900.png)

Note that now we have a 'Logout' button. Also, just the authorized will see the 'Delete' button, and be able to delete an image from the gallery, in the home page. If you click on 'Logout', you are no more logged in, and the home page will appear as following:

![image-20220609221800679](/home/leonardo/.config/Typora/typora-user-images/image-20220609221800679.png)

If you are logged in and hit 'Login' again, you will be redirected to the Accept/Reject page, without the need to set your credentials again.

If you hit 'Delete', the image object and its 'likes' and 'comments' objects associated are going to be deleted, immediately, as example: 

![image-20220609222031229](/home/leonardo/.config/Typora/typora-user-images/image-20220609222031229.png)

### Viewing, liking and comment

Either logged in or not, you can click on the 'View' button, and will be redirected to the following page:

![image-20220609222220157](/home/leonardo/.config/Typora/typora-user-images/image-20220609222220157.png)

In this view page, the picture associated will appear bigger, and the user, either logged in or not, can like and comment.

Note: In order to 'Like', you must fill the 'name' field with something. For instance:

![image-20220609222438328](/home/leonardo/.config/Typora/typora-user-images/image-20220609222438328.png)

Then hit the Like button:

![image-20220609222504462](/home/leonardo/.config/Typora/typora-user-images/image-20220609222504462.png)

If you do not provide something in the 'name' field, nothing will occur.

In order to comment something, you must fill the fields 'name' and 'comment', as following:

![image-20220609222657879](/home/leonardo/.config/Typora/typora-user-images/image-20220609222657879.png)

Then hit comment:

![image-20220609222725204](/home/leonardo/.config/Typora/typora-user-images/image-20220609222725204.png)



The 'like' and 'comment' counters will me incremented whenever a valid 'like' or 'comment' is done.

![image-20220609222835426](/home/leonardo/.config/Typora/typora-user-images/image-20220609222835426.png)

You may fill the comment and like areas, they are scrollable:

![image-20220609223246632](/home/leonardo/.config/Typora/typora-user-images/image-20220609223246632.png)

In order to return to the gallery, click on 'Return'.

# The code

The entire application has been built with these three components:

* Django
* MySQL
* AWS S3
* Google cloud platform

The web solution has been coded with django. Django is already a structure composed of models, views and templates. In the Django structure, there is also one called 'App'. In this application, the 'App' is called 'photo', and a specific folder for its contents is provided. 

In the models structure, three classes were developed: Pictures, Likes and Comments. Where each like/comment object is associated with a unique Picture. When such Picture object is deleted, the associated likes/comments objects are deleted. Each Picture, Like or Comment object is stored in a local SQL database, already built in with Django.

In the views structure, is where the routes are associated with functions. These functions perform the application logic, interaction with models, and renderization of templates.

In the templates structure, is where the HTML files play their role. Django also provides some special treatment with HTML files, enabling some interaction with some python objects inside the html file.

The AWS S3 is configured in the bottom of the settings.py file, located underneath the folder Custom_gallery. This project in this repository does not have AWS S3 configured by default, because there is a need to put some credentials, and I do not want to share my credentials in public. But the entire project is configured to, if you want to user AWS S3, just uncomment the following lines in the settings.py, fill them properly, and the application will already employ AWS S3.

![image-20220609230638915](/home/leonardo/.config/Typora/typora-user-images/image-20220609230638915.png)

When running locally, without these fields above filled, the application will understand that AWS is not set, and will store the pictures locally in the folder 'static'.

The deployed application in http://34.121.158.50/ employs AWS S3, just check the image link.

# Running the application locally

In order to run this application locally, clone this repository, and set a python virtual environment with the requirements.txt file provided inside the folder Custom_gallery. The python version employed by me has been 3.7.6, but much probably higher versions will also work.

So, step by step:

Clone this repository. Open a terminal and navigate to the same directory where the requirements.txt file is locates, i.e. custom-gallery/Custom_gallery/. 

Then, assuming you are using python 3.7.6, just install the requirements with:

```bash
pip3 install -r requirements.txt
```

Afterwards, still in the directory custom-gallery/Custom_gallery/. , type:

```bash
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py runserver
```

With the last command your server will be launched, and you should see something like:

![image-20220609232342906](/home/leonardo/.config/Typora/typora-user-images/image-20220609232342906.png)

Afterwards go to the browser in 127.0.0.1:8000 and the application will be working. If you upload some picture, it will be located underneath static/pictures folder. If you've set AWS S3, the pictures will be in your socket configured.
